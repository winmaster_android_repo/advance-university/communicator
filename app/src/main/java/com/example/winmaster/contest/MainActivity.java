package com.example.winmaster.contest;

import android.app.Activity;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.support.v4.util.ObjectsCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    public ListView listviewMessages;
    public Button buttonSend;
    public EditText editTextMessage;
    public  ArrayList<String> listOfMessages;
    public ArrayAdapter<String> adapterToList;
    public View currentView;
    public  boolean listEmptyFlag;

    public ReadMessages readMessagesClass;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listviewMessages = (ListView) findViewById(R.id.listviewMessages);
        buttonSend = (Button) findViewById(R.id.buttonSend);
        editTextMessage = (EditText) findViewById(R.id.editTextMessage);
        currentView = getWindow().getDecorView().getRootView();

        listOfMessages = new ArrayList<>();

        adapterToList= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listOfMessages );

        listviewMessages.setAdapter(adapterToList);

        ReadMessages();
        ReadMessagesStatus();
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.itemRefresh:
            {
               listOfMessages.clear();
               adapterToList.notifyDataSetChanged();

                ReadMessages();
                ReadMessagesStatus();

                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void ReadMessages()
    {
        readMessagesClass = new ReadMessages();
        readMessagesClass.execute();
    }

    public void ReadMessagesStatus()
    {
        if(readMessagesClass.getStatus()!= AsyncTask.Status.FINISHED)
        {
            try
            {
                Thread.sleep(2000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        readMessagesClass.cancelTask();

        listEmptyFlag = listOfMessages.isEmpty() ? true : false;
        adapterToList.notifyDataSetChanged();

        if(listEmptyFlag)
        {
            Toast.makeText(getApplicationContext(),"Włącz internet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void SendMessages(View view)
    {
        //Tworzenie nowego wątka asynchronicznego. Odpowiedź może trwać długo, dlatego nie można blokować wątka głównego
        if(!editTextMessage.getText().toString().equals(""))
        {
            AsyncTask.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        URL url = new URL("http://e-biuro.net/android10/messages/" + editTextMessage.getText().toString());
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("PUT");
                        connection.getResponseCode();
                    }
                    catch (MalformedURLException e)
                    {
                        e.printStackTrace();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
            Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_SHORT).show();
            editTextMessage.setText("");
        }
    }


    private class ReadMessages extends  AsyncTask
    {
        private boolean isTaskCancelled = false;

        public void cancelTask(){

            isTaskCancelled = true;
        }

        private boolean isTaskCancelled()
        {
            return isTaskCancelled;
        }

        @Override
        protected Object doInBackground(Object[] objects)
        {
            try
            {
                //Przygotowanie połączenia: url http://e-biuro.net/android10/, zasób messages
                URL service = new URL("http://e-biuro.net/android10/messages/");
                HttpURLConnection connection = (HttpURLConnection) service.openConnection();
                connection.setRequestMethod("GET");
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                String json = reader.readLine();
                JSONObject jObject = new JSONObject(json);

                JSONArray  messages = jObject.getJSONArray("Messages");

                for (int i=0 ; i<messages.length(); i++)
                {
                    listOfMessages.add(messages.getJSONObject(i).getString("message"));
                }

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (isTaskCancelled())
            {
                return null;
            }
            return null;
        }
    }
}


